#-------------------------------------------------
#
# Project created by QtCreator 2012-09-02T16:36:08
#
#-------------------------------------------------

QT       += core gui

TARGET = dlcd
TEMPLATE = app

win32 {
#LIBS += ENTER LIBS HERE FOR QWT!
#INCLUDEPATH += ENTER INCLUDE PATH HERE FOR QWT!
}
unix {
LIBS += -lqwt
CONFIG += qwt
}

SOURCES += main.cpp\
        dlcd.cpp \
    streamreader.cpp \
    plot.cpp \
    data.cpp \
    dial.cpp

HEADERS  += dlcd.h \
    streamreader.h \
    plot.h \
    data.h \
    dial.h








