#ifndef DIAL_H
#define DIAL_H

#include <QString>
#include <QKeyEvent>
#include <qwt_dial.h>

class Dial : public QwtDial
{
    Q_OBJECT

    QString m_label;

public:
    explicit Dial(QWidget *parent = 0);
    void setLabel(QString label);
    QString getLabel() const;

signals:

public slots:
    void setValue(double x, double y);

protected:
    void keyPressEvent(QKeyEvent *e);

};

#endif // DIAL_H
