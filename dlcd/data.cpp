#include "data.h"

Data::Data() : m_width(-1.0) {
    m_samples = new QVector<QPointF>();
    d_boundingRect.setCoords(1.0, 1.0, -2.0, -2.0);
}

void Data::append(double x, double y) {
    if (x > m_width && m_width > 0.0)
        d_boundingRect.setLeft(x-m_width);
    d_boundingRect.setRight(x);
    if (y > d_boundingRect.bottom())
        d_boundingRect.setBottom(y);
    if (y < d_boundingRect.top())
        d_boundingRect.setTop(y);

    m_samples->append(QPointF(x, y));
}

QRectF Data::boundingRect() const {
    return d_boundingRect;
}

size_t Data::size() const {
    return m_samples->size();
}

QPointF Data::sample(size_t i) const {
    return m_samples->at(i);
}

void Data::setBoundingWidth(double w) {
    m_width = w;
}

void Data::pop() {
    m_samples->pop_front();
}

void Data::clear() {
    m_samples->clear();
}

Data::~Data() {
    delete m_samples;
}
