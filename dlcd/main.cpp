#include <QtGui/QApplication>
#include <iostream>
#include "dlcd.h"

using namespace std;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QStringList args = a.arguments();
    if (args.size() < 2)
    {
        cout << "Usage: " << args.at(0).toStdString() << " <title>" << endl;
        cout << "While the windows are open:" << endl;
        cout << "\tPressing \'p\' will open the plot window." << endl;
        cout << "\tPressing \'d\' will open the dial." << endl;
        cout << "\tPressing \'q\' closes the program." << endl;
        cout << "\tPressing \'l\' writes the results to a log file. It starts logging the moment you press the key." << endl;
        cout << "\tPressing \'c\' clears the plot." << endl;
        cout << "\tPressing \'w\' sets the plot width." << endl;
        return -1;
    }
    dlcd w;
    w.setWindowTitle(args.at(1));
    w.show();

   return a.exec();
}
