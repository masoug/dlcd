#include "plot.h"
#include <QPalette>
#include <QInputDialog>
#include <qwt_plot_grid.h>
#include <qwt_plot_canvas.h>
#include <qwt_scale_engine.h>

Plot::Plot(QWidget *parent) :
    QwtPlot(parent)
{
    m_data = new Data();

    QwtPlotGrid *grid = new QwtPlotGrid();
    grid->setPen(QPen(Qt::gray, 0.0, Qt::DotLine));
    grid->enableX(true);
    grid->enableXMin(true);
    grid->enableY(true);
    grid->enableYMin(false);
    grid->attach(this);

    QPalette p = canvas()->palette();
    p.setBrush(QPalette::Window, QBrush(Qt::black));
    canvas()->setPalette(p);

    m_curve = new QwtPlotCurve();
    m_curve->setStyle(QwtPlotCurve::Lines);
    m_curve->setPen(QPen(Qt::green));
    m_curve->setData(m_data);
    m_curve->attach(this);

    replot();
}

void Plot::append(double x, double y){
    m_data->append(x, y);
    replot();
}

void Plot::setPlotWidth(double w) {
    m_data->setBoundingWidth(w);
}

void Plot::clear() {
    m_data->clear();
    replot();
}

void Plot::keyPressEvent(QKeyEvent *e) {
    if (e->key() == Qt::Key_W) {
        bool ok;
        double w = QInputDialog::getDouble(
                    this,
                    tr("Plot Width"),
                    tr("Enter plot width:"),
                    10.0, -214748364, 214748364,
                    2, &ok);
        if (ok)
            setPlotWidth(w);
        e->accept();
    } else if (e->key() == Qt::Key_C) {
        clear();
        e->accept();
    } else if (e->key() == Qt::Key_P && isVisible()) {
        close();
        e->accept();
    } else
        e->ignore();
}

Plot::~Plot() {
    delete m_data;
    delete m_curve;
}
