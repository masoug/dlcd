#ifndef STREAMREADER_H
#define STREAMREADER_H

#include <QThread>

class StreamReader : public QThread
{
    Q_OBJECT

    double m_x, m_y;

public:
    explicit StreamReader(QObject *parent = 0);

protected:
    void run();

signals:
    void data(double x, double y);

};

#endif // STREAMREADER_H
