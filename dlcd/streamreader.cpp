#include <iostream>
#include "streamreader.h"

using namespace std;

StreamReader::StreamReader(QObject *parent) :
    QThread(parent), m_x(0.0), m_y(0.0)
{
    start();
}

void StreamReader::run() {
    while (true) {
        cin >> m_x >> m_y;
        if (!cin.fail())
            emit data(m_x, m_y);
        else {
            cout << "INVALID DATA" << endl;
            // Somehow reset cin!
        }
    }
}
