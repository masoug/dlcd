#ifndef DATA_H
#define DATA_H

#include <qwt_series_data.h>
#include <QVector>

class Data : public QwtSeriesData<QPointF>
{
    QVector<QPointF> *m_samples;
    double m_width;

public:
    Data();
    void append(double x, double y);
    QRectF boundingRect() const;
    size_t size() const;
    QPointF sample(size_t i) const;
    void setBoundingWidth(double w);
    void pop();
    void clear();

    ~Data();
};

#endif // DATA_H
