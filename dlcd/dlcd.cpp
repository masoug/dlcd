#include <QFileDialog>
#include <QInputDialog>
#include <QStatusBar>
#include <QTextStream>
#include <QDateTime>
#include <iostream>
#include "dlcd.h"

using namespace std;

dlcd::dlcd(QWidget *parent)
    : QMainWindow(parent)
{
    m_lcd = new QLCDNumber(this);
    m_lcd->setStyleSheet(
                tr("QLCDNumber {background: black; color: #2AE714;}"));
    m_lcd->setMode(QLCDNumber::Dec);
    m_lcd->setSegmentStyle(QLCDNumber::Flat);
    m_lcd->setSmallDecimalPoint(true);
    //m_lcd->setDigitCount(10);
    //m_lcd->display(tr("3.00"));

    m_center = new QWidget(this);
    m_hBox = new QHBoxLayout(m_center);
    m_hBox->addWidget(m_lcd);
    m_center->setLayout(m_hBox);

    m_wheel = new QwtWheel(m_center);
    m_wheel->setOrientation(Qt::Vertical);
    m_wheel->setTickCnt(10);
    m_wheel->setRange(1.0, 10.0, 1.0);
    connect(m_wheel, SIGNAL(sliderMoved(double)), this, SLOT(setPrecision(double)));
    m_hBox->addWidget(m_wheel);

    m_stdin = new StreamReader(this);
    connect(m_stdin, SIGNAL(data(double, double)), this, SLOT(update(double, double)));

    m_plot = new Plot();
    connect(m_stdin, SIGNAL(data(double, double)), m_plot, SLOT(append(double, double)));

    m_dial = new Dial();
    connect(m_stdin, SIGNAL(data(double,double)), m_dial, SLOT(setValue(double,double)));

    m_log = new QFile(this);

    m_decWidth = 5; // Set the precision width of the LCD.

    /* Grab all keyboard events. */
    /* ERROR:
    X Error: BadWindow (invalid Window parameter) 3
    Major opcode: 31 (X_GrabKeyboard)
    Resource id:  0x0
    */
    //grabKeyboard();

    //setWindowTitle(tr("dlcd"));
    setCentralWidget(m_center);
}

void dlcd::update(double x, double y) {
    QString fmt;
    fmt.setNum(y, 'f', m_decWidth);
    m_lcd->display(fmt);
    if (m_log->exists()) {
        QTextStream l(m_log);
        l << x << ", " << y << "\n";
        m_log->flush();
    }
}

void dlcd::setPrecision(double p) {
    m_decWidth = (int)p;
    m_lcd->setDigitCount((int)p);
}

void dlcd::closeEvent(QCloseEvent *e) {
    /* Gracefully shut down the system. */
    m_dial->close();
    m_plot->close();
    m_stdin->terminate();
    m_stdin->wait();
    e->accept();
}

void dlcd::keyPressEvent(QKeyEvent *e) {
    if (e->key() == Qt::Key_P) { // open plot window
        m_plot->isVisible() ? m_plot->hide() : m_plot->show();
        m_plot->setWindowTitle(windowTitle()+" - Plot");
        e->accept();
    } else if (e->key() == Qt::Key_D) { // open dial window
        m_dial->isVisible() ? m_dial->hide() : m_dial->show();
        m_dial->setWindowTitle(windowTitle()+" - Dial");
        e->accept();
    }else if (e->key() == Qt::Key_Q) { // close
        close();
        e->accept();
    } else if (e->key() == Qt::Key_L && !m_log->exists()) { // start logging (if no tee)
        //        m_log->setFileName(QFileDialog::getSaveFileName(
        //                               this,
        //                               tr("Log Data to File"),
        //                               QString(),
        //                               tr("CSV Files (*.csv)")));
        m_log->setFileName(windowTitle()+".log");
        if (!m_log->open(QIODevice::WriteOnly |QIODevice::Append | QIODevice::Text))
            e->ignore();
        QTextStream l(m_log);
        l << "dlcd log file on " << QDateTime::currentDateTime().toString("ddd MMMM d yyyy h:m:s ap") << "\n";
        l << "CSV data for " << windowTitle() << "\n";
        m_log->flush();
        statusBar()->showMessage(tr("Logging data..."));
        e->accept();
    }
    else
        e->ignore();
    setFocus();
}

dlcd::~dlcd()
{
    m_log->close();
}
