#ifndef PLOT_H
#define PLOT_H

#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_series_data.h>
#include <QCloseEvent>
#include <QKeyEvent>
#include "data.h"

class Plot : public QwtPlot
{
    Q_OBJECT

    QwtPlotCurve *m_curve;
    Data *m_data;

public slots:
    void append(double x, double y);
    void setPlotWidth(double w);
    void clear();

public:
    Plot(QWidget *parent = NULL);
    ~Plot();

protected:
    void keyPressEvent(QKeyEvent *e);
};

#endif // PLOT_H
