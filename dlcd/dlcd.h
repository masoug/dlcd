#ifndef DLCD_H
#define DLCD_H

#include <QtGui/QMainWindow>
#include <QLCDNumber>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QThread>
#include <QCloseEvent>
#include <QKeyEvent>
#include <QFile>
#include <qwt_wheel.h>
#include "plot.h"
#include "dial.h"
#include "streamreader.h"

class dlcd : public QMainWindow
{
    Q_OBJECT

    QWidget *m_center;
    QHBoxLayout *m_hBox;
    QwtWheel *m_wheel;
    QLCDNumber *m_lcd;
    Plot *m_plot;
    Dial *m_dial;
    StreamReader *m_stdin;
    QFile *m_log;
    unsigned int m_decWidth;

public:
    dlcd(QWidget *parent = 0);
    ~dlcd();

public slots:
    void update(double x, double y);
    void setPrecision(double p);

protected:
    void closeEvent(QCloseEvent *e);
    void keyPressEvent(QKeyEvent *e);
};

#endif // DLCD_H
