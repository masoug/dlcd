#include <QInputDialog>
#include <qwt_dial_needle.h>
#include "dial.h"

Dial::Dial(QWidget *parent) :
    QwtDial(parent)
{
    setWrapping(false);
    setReadOnly(true);
    setOrigin(135.0);
    setScaleArc(0.0, 270.0);
    scaleDraw()->setSpacing(10);
    setScaleTicks(0, 2, 4);
    setRange(-1.0, 1.0);

    QwtDialSimpleNeedle *needle = new QwtDialSimpleNeedle(
                QwtDialSimpleNeedle::Arrow, true, Qt::red,
                QColor(Qt::gray).light(130));
    setNeedle(needle);
}

void Dial::setLabel(QString label) {
    m_label = label;
    update();
}

QString Dial::getLabel() const {
    return m_label;
}

void Dial::setValue(double x, double y) {
    QwtDial::setValue(y);
}

void Dial::keyPressEvent(QKeyEvent *e) {
    if (e->key() == Qt::Key_D && isVisible()) {
        close();
        e->accept();
    } else if (e->key() == Qt::Key_R) {
        bool ok, okk;
        double lo = QInputDialog::getDouble(
                    this,
                    tr("Lower Limit"),
                    tr("Enter dial lower limit:"),
                    minValue(), -214748364, 214748364,
                    2, &ok);
        double hi = QInputDialog::getDouble(
                    this,
                    tr("Upper Limit"),
                    tr("Enter dial upper limit:"),
                    maxValue(), -214748364, 214748364,
                    2, &okk);
        if (ok && okk) {
            setRange(lo, hi);
            scaleDraw()->setTickLength(QwtScaleDiv::MajorTick, 1.0);
            e->accept();
        } else
            e->ignore();
    }
}
